from import_export import resources
from usuarios.models import Usuario

class DocenteResource(resources.ModelResource):
    class Meta:
        model = Usuario