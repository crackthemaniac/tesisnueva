from django import forms
from usuario.models import Usuario

class ImportExcelForm(forms.Form):
    file  = forms.FileField(label= "Seleccione el Excel que desee subir")  