from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index_admin"),
    path('listar_usuarios', views.listar_usuarios, name="listar_usuarios"),
    path('agregar_usuarios', views.agregar_usuarios, name="agregar_usuarios"),
    path('editar_usuarios/<int:id>', views.editar_usuarios, name="editar_usuarios"),
    path('eliminar_usuarios/<int:id>', views.eliminar_usuarios, name="eliminar_usuarios"),
]