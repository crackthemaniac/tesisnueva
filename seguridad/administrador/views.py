from django.shortcuts import render, redirect
from django.http import HttpResponse
from usuarios.models import Usuario
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from usuarios.forms import UsuarioForm, testForm
from .resources import DocenteResource
from tablib import Dataset



# Create your views here.
def index(request):
	return render(request, 'administrador/index.html')

def listar_usuarios(request):
	#usuarios = Usuario.objects.filter(organizacion__nombre=request.user.usuario.organizacion.nombre)
	usuarios = Usuario.objects.all()
	return render(request, 'administrador/listar_usuarios.html', {'usuarios': usuarios})

def eliminar_usuarios(request, id):
	usuario = Usuario.objects.get(id=id)
	if request.method == 'POST':
		usuario.delete()
		return redirect('listar_usuarios')
	else:
		return render(request, 'administrador/borrar_usuarios.html', {'usuario': usuario})

def agregar_usuarios(request):
	contexto = {}
	if request.method == 'POST':
		form = testForm(request.POST)
		profile_form = UsuarioForm(request.POST)
		if form.is_valid() and profile_form.is_valid():
			usuario = form.save()
			perfil = profile_form.save(commit=False)
			perfil.usuario = usuario
			perfil.save()
		else:
			return render(request, 'administrador/agregar_usuarios.html', {'form': form, 'profile_form': profile_form})
		return redirect('listar_usuarios') 
		
	else:
		form = testForm()
		profile_form = UsuarioForm()

	contexto = {'form': form, 'profile_form': profile_form}

	return render(request, 'administrador/agregar_usuarios.html', contexto)

def editar_usuarios(request, id):
	usuario = Usuario.objects.get(id=id)
	user = User.objects.get(id=usuario.usuario.id)
	if request.method == 'POST':
		form =  testForm(request.POST, instance=user)
		profile_form = UsuarioForm(request.POST, instance=usuario)
		if form.is_valid() and profile_form.is_valid():
			form.save()
			profile_form.save()
			return redirect('/administrador/listar_usuarios')
		else:
			return render(request, '/administrador/listar_usuarios', {'form': form, 'profile_form': profile_form})
	else:
		form =  testForm(instance=user)
		profile_form = UsuarioForm(instance=usuario)
		context = {'form': form, 'profile_form': profile_form}
		return render(request, 'administrador/editar_usuarios.html', context)
















