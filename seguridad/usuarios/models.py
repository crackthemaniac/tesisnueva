from django.db import models
from django.contrib.auth.models import User
from docente.models import Carrera

class Usuario(models.Model):
	usuario = models.OneToOneField(User, on_delete=models.CASCADE)
	email = models.EmailField(max_length=144)
	facultad = models.OneToOneField(Carrera, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return self.usuario.username
