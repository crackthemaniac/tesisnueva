from django import forms
from .models import Usuario
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UsuarioForm(forms.ModelForm):


	
	email = forms.EmailField(widget=forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		))

	facultad = forms.Select()

	class Meta:
		model = Usuario
		fields = ('email','facultad')

class testForm(UserCreationForm):
	first_name = forms.CharField(widget=forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		))
	last_name = forms.CharField(widget=forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		))
	username = forms.CharField(widget=forms.TextInput(
			attrs = {
				'class': 'form-control',
			}
		))
	password1 = forms.CharField(widget=forms.PasswordInput(
			attrs = {
				'class': 'form-control',
			}
		))
	password2 = forms.CharField(widget=forms.PasswordInput(
			attrs = {
				'class': 'form-control',
			}
		))

	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'username', 'password1', 'password2')
		help_texts = {
			'username': None,
		}
