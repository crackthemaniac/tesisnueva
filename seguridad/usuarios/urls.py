from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name="index"),
	path('registrar_usuario/', views.registrar_usuario, name="registrar_usuario"),
	path('login/', views.iniciar_sesion, name="login"),
	path('logout/', views.cerrar_sesion, name="logout2"),
	path('agregar_usuario/', views.agregar_usuario, name="agregar_usuario")
]