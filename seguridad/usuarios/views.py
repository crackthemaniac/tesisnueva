from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import UsuarioForm
from django.contrib.auth.decorators import login_required


def index(request):
	return render(request, 'docente/index_usuario.html')

def iniciar_sesion(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username=username, password=password)
		if user:
			if user.is_staff:
				login(request, user)
				return redirect('index_admin')
			else:
				login(request, user)
				return redirect('index_gap')
		else:
			return render(request, 'usuarios/login.html')
	else:
		return render(request, 'usuarios/login.html')

def cerrar_sesion(request):
	logout(request)
	return redirect('login')

# Registro forma general
def registrar_usuario(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		profile_form = UsuarioForm(request.POST)
		if form.is_valid() and profile_form.is_valid():
			usuario = form.save()
			perfil = profile_form.save(commit=False)
			perfil.usuario = usuario
			perfil.save()
			nombre_usuario = form.cleaned_data['username']
			contrasena = form.cleaned_data['password1']
			usuario = authenticate(username=nombre_usuario, password=contrasena)
			login(request, usuario)
			return redirect('index_gap') 
	else:
		form = UserCreationForm()
		profile_form = UsuarioForm()

	contexto = {'form': form, 'profile_form': profile_form}

	return render(request, 'registration/register.html', contexto)

# Agregar usuario (función solo para admin)
@login_required(login_url='/administrador/')
def agregar_usuario(request):
	if request.method == 'POST':
		#form = UserCreationForm(request.POST)
		profile_form = UsuarioForm(request.POST)
		if profile_form.is_valid():
			#usuario = form.save()
			perfil = profile_form.save(commit=False)
			#perfil.usuario = usuario
			perfil.save()
			return redirect('index') 
	else:
		form = UserCreationForm()
		profile_form = UsuarioForm()

	contexto = {'form': form, 'profile_form': profile_form}

	return render(request, 'registration/register.html', contexto)


	