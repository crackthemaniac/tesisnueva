from django import forms
from .models import Respuesta

class SimpleForm(forms.Form):
	class Meta:
		model = Respuesta
		fields = ['opciones', 'encuestado']

class ImportExcelForm(forms.Form):
	file = forms.FileField(label= "Seleccione el Excel que desee subir")  