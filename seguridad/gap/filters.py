from .models import Pregunta
import django_filters


class PreguntaFilter(django_filters.FilterSet):
	dominio_choice = (
		('1', 'Politica de seguridad'),
		('2', 'organizacion de la seguridad informatica'),
		('3', 'Administracion activo'),
		('4', 'Seguridad Recursos Humanos'),
		('5', 'Seguridad Física y Ambiental'),
		('6', 'Administración Comunicaciones y Operaciones'),
		('7', 'Control de Accesos'),
		('8', 'Adquisición, desarrollo y mantenimiento de Sistemas Informáticos'),
		('9', 'Administración de Incidentes de Seguridad Informática'),
		('10', 'Administración Continuidad de Negocios'),
		('11', 'Cumplimiento'),
	)
	dominio = django_filters.ChoiceFilter(choices=dominio_choice)
	class Meta:
		model=Pregunta
		fields=['dominio',]

class MyCharFilter(django_filters.CharFilter):
	empty_value = 'EMPTY'

	dominio_choice = (
		('1', 'Politica de seguridad'),
		('2', 'organizacion de la seguridad informatica'),
		('3', 'Administracion activo'),
		('4', 'Seguridad Recursos Humanos'),
		('5', 'Seguridad Física y	 Ambiental'),
		('6', 'Administración Comunicaciones y Operaciones'),
		('7', 'Control de Accesos'),
		('8', 'Adquisición, desarrollo y mantenimiento de Sistemas Informáticos'),
		('9', 'Administración de Incidentes de Seguridad Informática'),
		('10', 'Administración Continuidad de Negocios'),
		('11', 'Cumplimiento'),
	)
	
	dominio = django_filters.ChoiceFilter(choices=dominio_choice)
	
	class Meta:
		model=Pregunta
		fields=['dominio',]
	
	def filter(self, qs, value):
		if value != self.empty_value:
			return super(MyCharFilter, self).filter(qs, value)
		qs = self.get_method(qs)(**{'%s__%s' % (self.name, self.lookup_expr): ""})
		return qs.distinct() if self.distinct else qs