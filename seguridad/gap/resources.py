from import_export import resources
from .models import Respuesta

class RespuestaResource(resources.ModelResource):
    class Meta:
        model = Respuesta