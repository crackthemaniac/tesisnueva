from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User
from .models import Pregunta, Respuesta, opciones
from .filters import PreguntaFilter, MyCharFilter
from .forms import SimpleForm
from django.db.models import Q
from django.db.models import Count

def index(request):
	return render(request, 'gap/index.html')

