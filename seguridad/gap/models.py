from django.db import models
from django.contrib.auth.models import User

class Pregunta(models.Model):
	dominio_choice = (
			('1', 'Politica de seguridad'),
			('2', 'organizacion de la seguridad informatica'),
			('3', 'Administracion activo'),
			('4', 'Seguridad Recursos Humanos'),
			('5', 'Seguridad Física y Ambiental'),
			('6', 'Administración Comunicaciones y Operaciones'),
			('7', 'Control de Accesos'),
			('8', 'Adquisición, desarrollo y mantenimiento de Sistemas Informáticos'),
			('9', 'Administración de Incidentes de Seguridad Informática'),
			('10', 'Administración Continuidad de Negocios'),
			('11', 'Cumplimiento'),
		)
	titulo = models.TextField(null=True, blank=True)
	dominio = models.CharField(max_length=144, choices=dominio_choice, default='')

	def __str__(self):
		return self.titulo

	@property
	def respuesta(self):
		return self.opciones_set.all()
	
class opciones(models.Model):
	pregunta = models.ForeignKey(Pregunta, null=True, blank=True, on_delete=models.CASCADE)
	voto_choice = (('Si','Si'),('No','No'),('No sé','No sé'),)
	voto = models.CharField(max_length=10, choices=voto_choice, default='No sé')

	def __str__(self):
		return '%s : ¿ %s ? : %s' % (self.pregunta.dominio, self.pregunta.titulo, self.voto)

class Respuesta(models.Model):
	opciones = models.ForeignKey(opciones, blank=True, null=True, on_delete=models.CASCADE)
	encuestado = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return '%s - %s' % (self.encuestado.username, self.opciones.pregunta.titulo)








