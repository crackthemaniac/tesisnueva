from django.contrib import admin
from .models import Pregunta, Respuesta, opciones
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class RespuestaResource(resources.ModelResource):

    class Meta:
        model = Respuesta



class PreguntaAdmin(admin.ModelAdmin):
	list_display = (
		'titulo',
		'dominio'
	)
	list_filter = ('dominio','titulo')

admin.site.register(Pregunta, PreguntaAdmin)


class RespuestaAdmin(ImportExportModelAdmin,admin.ModelAdmin):
	list_display = (
		'encuestado',
		'opciones'
	)
	list_filter = ('encuestado','opciones')
admin.site.register(Respuesta, RespuestaAdmin)


admin.site.register(opciones)
