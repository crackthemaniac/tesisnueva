from django.contrib import admin
from .models import *
from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from import_export.admin import ImportExportModelAdmin

class ContadorAdmin(admin.ModelAdmin):
	list_display = ('RUT', 'contador')
admin.site.register(Contador, ContadorAdmin)

class CarreraAdmin(admin.ModelAdmin):
	pass
admin.site.register(Carrera, CarreraAdmin)


class ProfesorResource(resources.ModelResource):
	class Meta:
		model = Profesor


class ProfesorAdmin(ImportExportModelAdmin,admin.ModelAdmin):
	list_display = ('id','RUT','nombre_docente','nombre_curso','NRC','email','P7','P1','P2','P3','P4','P5','P6','evaluacion_promedio','promedio_nota')
	resource_class = ProfesorResource

admin.site.register(Profesor, ProfesorAdmin)

