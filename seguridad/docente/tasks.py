from __future__ import absolute_import, unicode_literals
from django.conf import settings
from celery import shared_task
from django.core.mail import send_mail, EmailMessage
import os, sys


@shared_task
def send_personal_mail(rut, email, prefijo, host_url):
	try:
		
		template = "forms.html"
		asunto = "RV: RESULTADOS ENCUESTA DOCENTE 201910"
		email_from = settings.EMAIL_HOST_USER
		email_to = []
		email_to.append(email)
		email_mensaje= "Estimado(a) docente,\n\nJunto con saludar cordialmente, se hace entrega de los resultados obtenidos en la Encuesta de Evaluación Docente UNAB correspondiente al semestre y año vigente.\n\nPuede visualizar su reporte en linea con el siguiente link " + prefijo + host_url + "/reporte/"+rut+"\n\nComo institución educativa de primer nivel, buscamos promover la mejora continua en la calidad pedagógica y profesional de nuestros docentes, por tanto la información proporcionada es de suma importancia en la mejora de nuestros procesos internos de formación y acompañamiento al docente UNAB.\n\nEn esta oportunidad informamos las respuestas entregadas por sus estudiantes a cada una de las preguntas enunciadas, donde se presenta el porcentaje de logro obtenido en cada dimensión evaluada:\nP1: Cumplimiento programación Syllabus\nP2: Disposición favorable para el aprendizaje en clases\nP3: Dominio y conocimiento de la disciplina\nP4: Uso de metodologías que contribuyen al aprendizaje\nP5: Evaluación adecuada del aprendizaje\nP6: Motivación para el aprendizaje\nP7: Recomendación docente\n\nEsperamos que esta  información pueda ser de gran utilidad para la planificación de su que hacer pedagógico en el siguiente periodo.\nLe saluda atentamente,\nKaren Medina Muñoz\nDirectora Escuela de Derecho\nCarrera de Derecho\nFacultad de Derecho\nVicerrectoría Académica"
		msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
		msg.send()
		print('enviado')

		return True
	except Exception as e:
		print(e)
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		return False