from django.urls import path
from . import views


urlpatterns = [
    path('index/', views.index, name='index_usuario'),
    path('crear_docente/', views.crearDocente, name='crear_docente'),
    path('listar_docente/', views.listarDocente, name='listar_docente'),
    path('editar_docente/<id_profesor>', views.editarDocente, name='editar_docente'),
    path('eliminar_docente/<id_profesor>', views.eliminarDocente, name='eliminar_docente'),
    path('eliminar_todo/', views.eliminarTodo, name='eliminar_todo'),
    path('email/', views.Email, name='email'),
    path('import/', views.simple_upload, name='import'),
    path('contact/<id_profesor>', views.contact, name='contact'),
    path('acuso_recibo/', views.acusoRecibo, name='acuso_recibo'),
    path('reporte_online/', views.reporteOnline, name='reporte_online'),
    path('reporte/<id_profesor>', views.GeneratePdf.as_view(), name='reporte'),
    path('confirmar_eliminar_todo/', views.confirmar_eliminar_todo, name='confirmar_eliminar_todo'),
    path('enviarEmail/<id_profesor>', views.enviarEmail, name='enviarEmail'),
    path('async_send_mail/', views.async_enviarEmail, name='async_send_mail'),
]
