from django import forms
from docente.models import Profesor

class DocenteForm(forms.ModelForm):
	class Meta:
		model = Profesor
		fields = [
		'RUT',
		'nombre_docente',
		'nivel_formacion_academica',
		'tipo_contrato',
		'tipo_actividad',
		'periodo',
		'sede',
		'campus',
		'facultad_programa',
		'programa',
		'nombre_curso',
		'NRC',
		'n_alumnos',
		'n_alumnos_responden',
		'respuestas',
		'P1',
		'P2',
		'P3',
		'P4',
		'P5',
		'P6',
		'P7',
		'evaluacion_promedio',
		'promedio_nota',
		'email',
		]
		labels = {
		'nombre': 'Nombre',
		'rut': 'RUT',
		'email': 'E-mail',
		}


"""
class ProfesorForm(forms.ModelForm):
	class Meta:
		model = Profesor
		fields = ['RUT',
		'nombre_docente',
		'nivel_formacion_academica',
		'tipo_contrato',
		'tipo_actividad',
		'periodo',
		'sede',
		'campus',
		'facultad_programa',
		'programa',
		'nombre_curso',
		'NRC',
		'n_alumnos',
		'n_alumnos_responden',
		'respuestas',
		'P7',
		'P1',
		'P2',
		'P3',
		'P4',
		'P5',
		'P6',
		'evaluacion_promedio',
		'promedio_nota',
		]
"""

class ImportExcelForm(forms.Form):
    file  = forms.FileField(label= "Seleccione el Excel que desee subir")  