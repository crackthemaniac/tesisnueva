from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from docente.forms import DocenteForm
from docente.models import Profesor, Contador, Carrera
from .resources import ProfesorResource
from tablib import Dataset
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.views.generic import View
from docente.utils import render_to_pdf
from django.template.loader import get_template
from django.db.models import Count
from docente.tasks import send_personal_mail as async_send_mail


def index(request):
	return render(request, 'index.html')

def crearDocente(request):
	if request.method == 'POST':
		docente_form = DocenteForm(request.POST)
		if docente_form.is_valid():
			docente_form.save()
		return redirect('listar_docente')
	else:
		docente_form = DocenteForm()
	return render(request,'crear_docente.html', {'docente_form':docente_form})

def listarDocente(request):
	profesor = Profesor.objects.all()
	contexto = {'profesores': profesor}
	return render(request, 'listar_docente.html', contexto)

def editarDocente(request, id_profesor):
	profesor = Profesor.objects.get(id=id_profesor)
	if request.method == 'GET':
		docente_form = DocenteForm(instance=profesor)
	else:
		docente_form = DocenteForm(request.POST, instance=profesor)
		if docente_form.is_valid():
			docente_form.save()
		return redirect('listar_docente')
	return render(request, 'crear_docente.html', {'docente_form':docente_form})

def eliminarDocente(request, id_profesor):
	profesor = Profesor.objects.get(id=id_profesor)
	if request.method == 'POST':
		#query = Contador.objects.get(RUT=profesor.RUT)
		#query.delete()
		profesor.delete()
		return redirect('listar_docente')
	return render(request, 'eliminar_docente.html', {'profesor': profesor})

def eliminarTodo(request):
	if request.method == 'POST':
		profesor = Profesor.objects.all().delete()
		contexto = {'profesores': profesor}
		return redirect('listar_docente')
	return render(request, 'eliminar_todo.html')

class GeneratePdf(View):
	def get(self, request, id_profesor, *args, **kwargs):
		#profesor = Profesor.objects.get(RUT="70487373")
		if request.method == 'GET':
			profesor = Profesor.objects.filter(RUT=id_profesor)
			profesor_porcentaje = [profe.cambiar_porcentaje() for profe in profesor]
			contexto = {'profesores': profesor_porcentaje}
		pdf = render_to_pdf('invoice.html', contexto)
		try:
			cont = Contador.objects.get(RUT=id_profesor)
		except Contador.DoesNotExist:
			cont =  Contador.objects.create(RUT=id_profesor)
		cont.aumentar_contador()
		# Profe = Profesor.objects.filter(RUT=cont.RUT).fist()
		return HttpResponse(pdf, content_type='application/pdf')

def reporteOnline(request):
	try:
		profesor = Profesor.objects.filter(programa=request.user.usuario.facultad)
		lista = []
		validar = []
		for profe in profesor:
			if profe.RUT not in validar:
				lista.append(profe)
				validar.append(profe.RUT)
		contexto = {'profesores': lista}
		return render(request, 'reporte_online.html', contexto)

	except:

		profesor = Profesor.objects.all()
		lista = []
		validar = []
		for profe in profesor:
			if profe.RUT not in validar:
				lista.append(profe)
				validar.append(profe.RUT)
		contexto = {'profesores': lista}
		return render(request, 'reporte_online.html', contexto)


def acusoRecibo(request):
	contadores = Contador.objects.all()
	contexto = {'contadores': contadores}
	return render(request, 'acuso_recibo.html', contexto)

"""

def acusoRecibo(request):
	try: 
		contadores_total = Contador.objects.all() 
		profesor = Profesor.objects.filter(programa=request.user.usuario.facultad)
		lista_ruts = []
		lista_final = []
		for p in profesor:
			if p.RUT not in lista_ruts:
				lista_ruts.append(p.RUT)


		for r in lista_ruts: 
			contador_get = Contador.objects.get(RUT=r) 		
			lista_final.append(contador_get)

		contexto = {'contadores': lista_final, 'profesores':profesor}
		return render(request, 'acuso_recibo.html', contexto)

	except:
		profesor = Profesor.objects.all()
		contadores = Contador.objects.all()
		contexto = {'contadores': contadores, 'profesores':profesor}
		return render(request, 'acuso_recibo.html', contexto)
"""


def contact(request, id_profesor):
	#AGREGAR TRY Y EXCEPTION
	"""template = "forms.html"
				asunto = "RV: RESULTADOS ENCUESTA DOCENTE"
				email_from = settings.EMAIL_HOST_USER
				email_to = []
			
				profesor = Profesor.objects.get(RUT=id_profesor)
			
				if request.method == 'GET':
					profesor = Profesor.objects.filter(RUT=id_profesor).values('email')
					
					for i in profesor:
						email_to.append(i['email'])
				
				contexto = {'profesores': profesor}
			
					
				pdf = render_to_pdf('invoice.html', contexto)
				email_mensaje = "Estimado(a) docente,\n\nJunto con saludar cordialmente, la Facultad de Derecho hace entrega de los resultados obtenidos en la Encuesta de Evaluación Docente UNAB correspondiente al semestre 2, 2018.\nUsted ha logrado un 57% en nivel de recomendación, lo que constituye un Nivel de desempeño Aceptable, de acuerdo a la escala de calificación UNAB.\nComo institución educativa de primer nivel, buscamos promover la mejora continua en la calidad pedagógica y profesional de nuestros docentes, por tanto la información proporcionada es de suma importancia en la mejora de nuestros procesos internos de formación y acompañamiento al docente UNAB.\nEn esta oportunidad informamos las respuestas entregadas por sus estudiantes a cada una de las preguntas enunciadas, donde se presenta el porcentaje de logro obtenido en cada dimensión evaluada:\nP1: Cumplimiento programación Syllabu\nP2: Disposición favorable para el aprendizaje en clase\nP3: Dominio y conocimiento de la disciplin\nP4: Uso de metodologías que contribuyen al aprendizaj\nP5: Evaluación adecuada del aprendizaj\nP6: Motivación para el aprendizaj\nP7: Recomendación docente\nEsperamos que esta información pueda ser de gran utilidad para la planificación de su quehacer pedagógico en el siguiente periodo.\nLe saluda atentamente,\nKaren Medina Muñoz\nDirectora Escuela de Derecho\nCarrera de Derecho\nFacultad de Derecho\nVicerrectoría Académica"
				
				msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
				#msg.attach_file(report_pdf.name)
				#msg.content_subtype = "html"  
				#msg.attach("invoicex.pdf", pdf , "application/pdf")
				msg.send()"""
	pass
	return render(request, template)

def export(request):
    docente_resource = ProfesorResource()
    dataset = docente_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="docentes.xls"'
    return response

def confirmar_eliminar_todo(request):

    template_name = 'confirmar_eliminar_todo.html'

    return render(request, template_name, {})

def simple_upload(request):
    if request.method == 'POST':
        docente_resource = ProfesorResource()
        dataset = Dataset()
        new_docente = request.FILES['myfile']

        imported_data = dataset.load(new_docente.read())
        result = docente_resource.import_data(dataset, dry_run=True)  # Test the data import
        #

        if not result.has_errors():
            docente_resource.import_data(dataset, dry_run=False)  # Actually import now
            profesores = Profesor.objects.all()
            carreras_actual = []
            carreras_db = []

            carreras_query = Carrera.objects.all()

            for q in carreras_query:
            	carreras_db.append(q.nombre)	

            for i in profesores:

            	if i.programa not in carreras_db:
            		if i.programa not in carreras_actual:
            			carreras_actual.append(i.programa)
            			obj = Carrera(nombre=i.programa)
            			obj.save()
            return redirect('listar_docente')


    return render(request, 'import.html')

def Email(request):

	try:
		profesor = Profesor.objects.filter(programa=request.user.usuario.facultad)
		lista = []
		validar = []
		for profe in profesor:
			if profe.RUT not in validar:
				lista.append(profe)
				validar.append(profe.RUT)
		contexto = {'profesores': lista}
		return render(request, 'email.html', contexto)
	except:
		profesor = Profesor.objects.all()
		lista = []
		validar = []
		for profe in profesor:
			if profe.RUT not in validar:
				lista.append(profe)
				validar.append(profe.RUT)
		contexto = {'profesores': lista}
		return render(request, 'email.html', contexto)



def enviarEmail(request, id_profesor, *args, **kwargs):
	profesor = Profesor.objects.filter(RUT=id_profesor)
	rut=""
	email=""
	for i in profesor:
		rut=i.RUT
		email=i.email
	template = "forms.html"
	asunto = "RV: RESULTADOS ENCUESTA DOCENTE 201910"
	email_from = settings.EMAIL_HOST_USER
	email_to = []
	email_to.append(email)
	email_mensaje= "Estimado(a) docente,\n\nJunto con saludar cordialmente, se hace entrega de los resultados obtenidos en la Encuesta de Evaluación Docente UNAB correspondiente al semestre y año vigente.\n\nPuede visualizar su reporte en linea con el siguiente link http://localhost:8000/reporte/"+rut+"\n\nComo institución educativa de primer nivel, buscamos promover la mejora continua en la calidad pedagógica y profesional de nuestros docentes, por tanto la información proporcionada es de suma importancia en la mejora de nuestros procesos internos de formación y acompañamiento al docente UNAB.\n\nEn esta oportunidad informamos las respuestas entregadas por sus estudiantes a cada una de las preguntas enunciadas, donde se presenta el porcentaje de logro obtenido en cada dimensión evaluada:\nP1: Cumplimiento programación Syllabus\nP2: Disposición favorable para el aprendizaje en clases\nP3: Dominio y conocimiento de la disciplina\nP4: Uso de metodologías que contribuyen al aprendizaje\nP5: Evaluación adecuada del aprendizaje\nP6: Motivación para el aprendizaje\nP7: Recomendación docente\n\nEsperamos que esta  información pueda ser de gran utilidad para la planificación de su que hacer pedagógico en el siguiente periodo.\nLe saluda atentamente,\nKaren Medina Muñoz\nDirectora Escuela de Derecho\nCarrera de Derecho\nFacultad de Derecho\nVicerrectoría Académica"
	msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
	msg.send()
	
	return render(request, template)

def async_enviarEmail(request, *args, **kwargs):
	profesor = Profesor.objects.filter(RUT=request.POST['rut'])
	rut=""
	email=""
	for i in profesor:
		rut=i.RUT
		email=i.email
	prefijo = 'https://' if request.is_secure() else 'http://'
	tarea_asincrona = async_send_mail.delay(rut, email, prefijo, str(request.META['HTTP_HOST']))

	
	#return render(request, 'enviando.html')

	return JsonResponse({'enviado':('ok' if tarea_asincrona else 'error')})


"""
#QUEDE AQUI, NECESITO ENVIAR A TODOS LOS DOCENTES EL LINK
def enviarTodo(request):
	rut=""
	email=""
	template = "forms.html"
	asunto = "RV: RESULTADOS ENCUESTA DOCENTE 201910"
	email_from = settings.EMAIL_HOST_USER
	email_to = []
	email_to1 = []
	email_to2 = []
	rut1=[]
	rut2=[]

	profesor = Profesor.objects.values('email','RUT')

	for i in profesor:
		email_to1.append(i['email'])
		rut1.append(i['RUT'])

	for j in email_to1:
		if j not in email_to:
			email_to.append(j)

	for k in rut1:
		if k not in rut2:
			rut2.append(k)

	
	#rut="hhhh"
	#email_mensaje= "Estimado(a) docente,\n\nJunto con saludar cordialmente, se hace entrega de los resultados obtenidos en la Encuesta de Evaluación Docente UNAB correspondiente al semestre y año vigente.\n\nPuede visualizar su reporte en linea con el siguiente link http://localhost:3000/reporte/"+rut+"\n\nComo institución educativa de primer nivel, buscamos promover la mejora continua en la calidad pedagógica y profesional de nuestros docentes, por tanto la información proporcionada es de suma importancia en la mejora de nuestros procesos internos de formación y acompañamiento al docente UNAB.\n\nEn esta oportunidad informamos las respuestas entregadas por sus estudiantes a cada una de las preguntas enunciadas, donde se presenta el porcentaje de logro obtenido en cada dimensión evaluada:\nP1: Cumplimiento programación Syllabus\nP2: Disposición favorable para el aprendizaje en clases\nP3: Dominio y conocimiento de la disciplina\nP4: Uso de metodologías que contribuyen al aprendizaje\nP5: Evaluación adecuada del aprendizaje\nP6: Motivación para el aprendizaje\nP7: Recomendación docente\n\nEsperamos que esta  información pueda ser de gran utilidad para la planificación de su que hacer pedagógico en el siguiente periodo.\nLe saluda atentamente,\nKaren Medina Muñoz\nDirectora Escuela de Derecho\nCarrera de Derecho\nFacultad de Derecho\nVicerrectoría Académica"
	email_mensaje="hola"
	msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
	msg.send()
	
	return render(request, template)
"""

















""" 
	template = "forms.html"
				asunto = "RV: RESULTADOS ENCUESTA DOCENTE 201910"
				email_from = settings.EMAIL_HOST_USER
				email_to = ['el_robert_crack@hotmail.com']
			
				profesor = Profesor.objects.get(RUT=id_profesor)
				
				if request.method == 'GET':
					profesor = Profesor.objects.filter(RUT=id_profesor).values('email')
					
					for i in profesor:
						email_to.append(i['email'])
						contexto = {'profesores': email_to}
			
				email_mensaje = "hola"
				msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
				#msg.attach_file(report_pdf.name)
				#msg.content_subtype = "html"  
				#msg.attach("invoicex.pdf", pdf , "application/pdf")
				msg.send()
				return render(request, template, contexto)"""


"""
CON ESTE METODO RECORRO LOS EMAIL DE LOS DOCENTES Y ENVIO EL MISMO MENSAJE A TODOS LOS DOCENTES

def contact(request):
	template = "forms.html"
	asunto = "Asunto de prueba 4"
	email_from = settings.EMAIL_HOST_USER
	email_to = []

	docente = Docente.objects.values('email')

	for i in docente:
		email_to.append(i['email'])

	email_mensaje = "Este es el mensaje de pruebaa"

	send_mail(asunto, email_mensaje, email_from, email_to, fail_silently=False)
	
	return render(request, template)
"""


"""
#CON ESTE METODO, ENVIO UN ARCHIVO ADJUNTO A TODOS LOS DOCENTES
def contact(request):
	template = "forms.html"
	asunto = "Asunto de prueba 5"
	email_from = settings.EMAIL_HOST_USER
	email_to = []

	docente = Docente.objects.values('email')

	for i in docente:
		email_to.append(i['email'])

	email_mensaje = "Este es el mensaje de pruebaa 5"

	msg = EmailMessage(asunto, email_mensaje, email_from, email_to)
	msg.content_subtype = "html"  
	msg.attach_file('mantenedor/prueba.xlsx')
	msg.send()

	
	return render(request, template)
"""




"""
	lista=[]
	for i in docente:
		lista.append(i['email'])

	contexto = {'docentes': lista}
	return render(request, 'email.html', contexto)
"""

"""
class GeneratePDF(View):
    def get(self, request, *args, **kwargs):
        template = get_template('invoice.html')
        context = {
            "invoice_id": 123,
            "customer_name": "John Cooper",
            "amount": 1399.99,
            "today": "Today",
        }
        html = template.render(context)
        pdf = render_to_pdf('invoice.html', context)
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_%s.pdf" %("12341231")
            content = "inline; filename='%s'" %(filename)
            download = request.GET.get("download")
            if download:
                content = "attachment; filename='%s'" %(filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")

"""


"""

TEXTO PAL CORREO

Estimado(a) docente,

Junto con saludar cordialmente, la Facultad de Derecho hace entrega de los resultados obtenidos en la Encuesta de Evaluación Docente UNAB correspondiente al semestre 2, 2018.

Usted ha logrado un 57% en nivel de recomendación, lo que constituye un Nivel de desempeño Aceptable, de acuerdo a la escala de calificación UNAB.

Como institución educativa de primer nivel, buscamos promover la mejora continua en la calidad pedagógica y profesional de nuestros docentes, por tanto la información proporcionada es de suma importancia en la mejora de nuestros procesos internos de formación y acompañamiento al docente UNAB.

En esta oportunidad informamos las respuestas entregadas por sus estudiantes a cada una de las preguntas enunciadas, donde se presenta el porcentaje de logro obtenido en cada dimensión evaluada:

P1: Cumplimiento programación Syllabus
P2: Disposición favorable para el aprendizaje en clases
P3: Dominio y conocimiento de la disciplina
P4: Uso de metodologías que contribuyen al aprendizaje
P5: Evaluación adecuada del aprendizaje
P6: Motivación para el aprendizaje
P7: Recomendación docente

Esperamos que esta información pueda ser de gran utilidad para la planificación de su quehacer pedagógico en el siguiente periodo.

Le saluda atentamente,

Karen Medina Muñoz
Directora Escuela de Derecho
Carrera de Derecho
Facultad de Derecho
Vicerrectoría Académica


"""