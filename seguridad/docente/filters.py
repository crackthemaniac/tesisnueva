import django_filters
from .models import Profesor

class ProfesorFilter(django_filters.FilterSet):


	class Meta:
		model = Profesor
		fiels = ('RUT', 'nombre_docente', 'facultad_programa', 'programa')
