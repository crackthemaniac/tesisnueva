from django.db import models


class Profesor(models.Model):
	RUT = models.CharField(max_length=20, blank=True, null=True)
	nombre_docente = models.CharField(max_length=100, blank=True, null=True)
	nivel_formacion_academica = models.CharField(max_length=50, blank=True, null=True)
	tipo_contrato = models.CharField(max_length=30, blank=True, null=True)
	tipo_actividad = models.CharField(max_length=30, blank=True, null=True)
	periodo = models.CharField(max_length=30, blank=True, null=True)
	sede = models.CharField(max_length=30, blank=True, null=True)
	campus = models.CharField(max_length=30, blank=True, null=True)
	facultad_programa = models.CharField(max_length=30, blank=True, null=True)
	programa = models.CharField(max_length=30, blank=True, null=True)
	nombre_curso = models.CharField(max_length=50, blank=True, null=True)
	NRC = models.CharField(max_length=30, blank=True, null=True)
	n_alumnos = models.IntegerField()
	n_alumnos_responden = models.IntegerField()
	respuestas = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True)
	P7 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P1 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P2 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P3 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P4 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P5 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	P6 = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	evaluacion_promedio = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	promedio_nota = models.DecimalField(max_digits=5, decimal_places=3, blank=True, null=True, default=0)
	email = models.EmailField(max_length=144, blank=True, null=True)

	def cambiar_porcentaje(self):
		self.P1 = round(self.P1*100,1)
		self.P2 = round(self.P2*100,1)
		self.P3 = round(self.P3*100,1)
		self.P4 = round(self.P4*100,1)
		self.P5 = round(self.P5*100,1)
		self.P6 = round(self.P6*100,1)
		self.P7 = round(self.P7*100,1)
		self.respuestas = round(self.respuestas*100,1)
		self.evaluacion_promedio = round(self.evaluacion_promedio*100,1)

		return self

	def __str__(self):
		return self.RUT

class Carrera(models.Model):
	nombre = models.CharField(max_length=100)

	def __str__(self):
		return self.nombre


class Contador(models.Model):
	RUT = models.CharField(max_length=20, unique=True)
	contador = models.PositiveIntegerField(default=0)

	def aumentar_contador(self):
		self.contador += 1
		self.save()




		