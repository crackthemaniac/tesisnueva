from import_export import fields, resources
from import_export.widgets import ForeignKeyWidget
from .models import  Profesor


"""class DocenteResource(resources.ModelResource):
	facultad = fields.Field(
		column_name='facultad',
		attribute='facultad',
		widget=ForeignKeyWidget(Facultad, 'nombre'))

	carrera = fields.Field(
		column_name='carrera',
		attribute='carrera',
		widget=ForeignKeyWidget(Carrera, 'nombre'))

	class Meta:
		model = Docente
"""

class ProfesorResource(resources.ModelResource):
	class Meta:
		model = Profesor